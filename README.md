# README #

FunFacts is an Android app developed for beginners in Android app development to understand basic functionalities in an Activity

### What is this repository for? ###

This repository is for Android app development to understand basic functionalities in an Activity


### How do I get set up? ###

1.	Import project into Android Studio

2.	Open the SDK manager to install the required Android SDK Tools and Android SDK Build-tools.

3.	There are no Third party libraries or Gradle dependencies used.

4.	minSdkVersion 14 targetSdkVersion 23

5.	Build the Project.

6.	Compile and Run


**Configuration**- Android Studio IDE, Genymotion (Simulator)